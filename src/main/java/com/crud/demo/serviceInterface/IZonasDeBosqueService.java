package com.crud.demo.serviceInterface;

import com.crud.demo.modelo.Bosque;
import com.crud.demo.modelo.ZonasDeBosque;

import java.util.List;
import java.util.Optional;

public interface IZonasDeBosqueService {
    public List<ZonasDeBosque> listarZona();

    public Optional<ZonasDeBosque> listarIdZona(int idZona);

    public int saveZona(ZonasDeBosque p);

    public void deleteZona(int idZona);
}
