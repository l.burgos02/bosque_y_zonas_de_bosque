package com.crud.demo.serviceInterface;

import java.util.List;
import java.util.Optional;

import com.crud.demo.modelo.Bosque;

public interface IBosqueService {
	public List<Bosque> listar();

	public Optional<Bosque> listarId(int id);

	public int save(Bosque p);

	public void delete(int id);
}
