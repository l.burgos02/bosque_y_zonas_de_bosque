package com.crud.demo.modelo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IncidentesDTO {
    private Long id;
    private String titulo;
    private String fechayhora;
    private String ubicacion;
    private String estado;
    private String categoria_id;
    private String id_zona;

    public IncidentesDTO toIncidentes(){
        return new IncidentesDTO(
                this.id,
                this.titulo,
                this.fechayhora,
                this.ubicacion,
                this.estado,
                this.categoria_id,
                this.id_zona
        );
    }
}
