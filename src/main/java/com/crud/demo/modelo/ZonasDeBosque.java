package com.crud.demo.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
@Entity
@Table(name="ZonadeBosque")
public class ZonasDeBosque {


        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int idZona;

        private String arboles;
        private int numeroZona;

        private String ubicacionZona;

        @JsonIgnore
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="id")
        private Bosque bosque;
        public ZonasDeBosque() {
            // TODO Auto-generated constructor stub
        }
        public ZonasDeBosque(int idZona, String arboles,int numeroZona, String ubicacionZona) {
            super();
            this.idZona = idZona;
            this.arboles = arboles;
            this.numeroZona = numeroZona;
            this.ubicacionZona = ubicacionZona;

        }

    public String getUbicacionZona() {
        return ubicacionZona;
    }

    public void setUbicacionZona(String ubicacionZona) {
        this.ubicacionZona = ubicacionZona;
    }

    public int getIdZona() {
        return idZona;
    }

    public void setIdZona(int idZona) {
        this.idZona = idZona;
    }

    public String getArboles() {
        return arboles;
    }

    public void setArboles(String arboles) {
        this.arboles = arboles;
    }

    public int getNumeroZona() {
        return numeroZona;
    }

    public void setNumeroZona(int numeroZona) {
        this.numeroZona = numeroZona;
    }
}
