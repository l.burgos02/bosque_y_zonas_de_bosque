package com.crud.demo.service;




import com.crud.demo.modelo.IncidentesDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.List;

@Service
public class ExternalApi {


    @Value("${urlbackend}")
    private String urlbackend;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    Gson gson;

    public String fetchUserData(){
        try {
            return restTemplate.getForObject(urlbackend, String.class);

        } catch (Exception abc){
            throw new RuntimeException("Error al obtener datos de la API");
        }
    }

    public List<IncidentesDTO> getincidente(){

        try {
            var incidentesRaw = restTemplate.getForObject(urlbackend, String.class);
            ObjectMapper mapper = new ObjectMapper();

            Object[] incidentesDTOS = mapper.readValue(incidentesRaw, Object[].class);
            List<IncidentesDTO> incidentes = Arrays.asList(mapper.readValue(incidentesRaw.toLowerCase(), IncidentesDTO[].class));

            for (Object obj : incidentesDTOS){
                if(obj instanceof IncidentesDTO){
                    IncidentesDTO incidente = (IncidentesDTO) obj;
                    incidentes.add(incidente);
                }
            }
            return incidentes;

        } catch (RestClientException abc){
            throw new RuntimeException("Error al obtener datos de la API");
        } catch (JsonMappingException e) {
            throw new RuntimeException(e);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


}