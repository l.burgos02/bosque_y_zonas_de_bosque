package com.crud.demo.service;

import com.crud.demo.modelo.Bosque;
import com.crud.demo.modelo.ZonasDeBosque;
import com.crud.demo.modeloDAO.IBosque;
import com.crud.demo.modeloDAO.IZonasDeBosque;
import com.crud.demo.serviceInterface.IZonasDeBosqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class ZonasDeBosquesService implements IZonasDeBosqueService {
    @Autowired
    private IZonasDeBosque dao1;

    @Override
    public List<ZonasDeBosque> listarZona() {
        return (List<ZonasDeBosque>) dao1.findAll();
    }

    @Override
    public Optional<ZonasDeBosque> listarIdZona(int idZona) {
        return dao1.findById(idZona);
    }

    @Override
    public int saveZona(ZonasDeBosque pz) {
        int res1=0;
        ZonasDeBosque per=dao1.save(pz);
        if(!per.equals(null)) {
            res1=1;
        }
        return res1;
    }

    @Override
    public void deleteZona(int idZona) {
        dao1.deleteById(idZona);

    }

}
