package com.crud.demo.service;

import com.crud.demo.modelo.Bosque;
import com.crud.demo.modeloDAO.IBosque;
import com.crud.demo.serviceInterface.IBosqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BosqueService implements IBosqueService {

	@Autowired
	private IBosque dao;
	
	@Override
	public List<Bosque> listar() {
		return (List<Bosque>) dao.findAll();
	}

	@Override
	public Optional<Bosque> listarId(int id) {
		return dao.findById(id);
	}

	@Override
	public int save(Bosque p) {
		int res=0;
		Bosque per=dao.save(p);
		if(!per.equals(null)) {
			res=1;
		}
		return res;
	}

	@Override
	public void delete(int id) {
		dao.deleteById(id);
		
	}

}
