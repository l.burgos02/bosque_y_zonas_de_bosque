package com.crud.demo.controler;

import javax.validation.Valid;


import com.crud.demo.modelo.IncidentesDTO;
import com.crud.demo.modelo.ZonasDeBosque;
import com.crud.demo.service.ExternalApi;
import com.crud.demo.serviceInterface.IZonasDeBosqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.crud.demo.modelo.Bosque;
import com.crud.demo.serviceInterface.IBosqueService;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping
/**
 * Controlador principal despliega todas las URI con los datos de las tablas
 */
public class BosqueController {
	
	@Autowired
	private IBosqueService service;

	@Autowired
	private IZonasDeBosqueService service1;

	@Autowired
	ExternalApi externalApi;

	@CrossOrigin(origins = {
			"http://200.13.4.249:8080"

	},
			allowedHeaders = "X-AUTH-TOKEN",
			allowCredentials = "false",
			maxAge = 15 * 60,
			methods = {
					RequestMethod.GET,
					RequestMethod.POST
			})
	/**	//Bosque controller */






	@GetMapping("/")
	public String listar(Model model) {
		List<Bosque> bosques=service.listar();
		List<ZonasDeBosque> zonasDeBosques=service1.listarZona();

		model.addAttribute("zonasDeBosques", zonasDeBosques);
		model.addAttribute("bosques", bosques);
		return "index";
	}
	@GetMapping("/listarIncidentesYZonas")
	public String listarIncidentesyZonas(Model model){
		List<ZonasDeBosque> zonasDeBosques=service1.listarZona();
		List<IncidentesDTO> incidentes = externalApi.getincidente()
				.stream()
				.map(IncidentesDTO::toIncidentes)
				.collect(Collectors.toList());
		model.addAttribute("incidentes",incidentes);
		model.addAttribute("zonasDeBosques", zonasDeBosques);
		return "IncidentesYZonas";
	}
	@GetMapping("/listar/{id}")
	public String listarId(@PathVariable int id,Model model) {
		model.addAttribute("bosques", service.listarId(id));
		return "form";
	}
	
	@GetMapping("/new")
	public String nuevo(Model model) {
		model.addAttribute("bosques", new Bosque());
		return "form";
	}
	
	@PostMapping("/save")
	public String save(@Valid Bosque p, Model model) {
		int id=service.save(p);
		if(id==0) {
			return "form";
		}
		return "redirect:/";
	}
	
	@GetMapping("/delete/{id}")
	public String eliminar(@PathVariable int id,Model model) {
		service.delete(id);
		return "redirect:/";
	}

	/**
	 * //Zona de Bosque Controller

	 */

	@GetMapping("/listarZona/{idZona}")
	public String listarIdZona(@PathVariable int idZona, Model model) {
		model.addAttribute("zonasDeBosques", service1.listarIdZona(idZona));
		return "formZona";
	}

	@GetMapping("/newZona")
	public String nuevoZona(Model model) {
		model.addAttribute("zonasDeBosques", new ZonasDeBosque());
		return "formZona";
	}

	@PostMapping("/saveZona")
	public String saveZona(@Valid ZonasDeBosque pz, Model model) {
		int idZona=service1.saveZona(pz);
		if(idZona==0) {
			return "formZona";
		}
		return "redirect:/";
	}

	@GetMapping("/deleteZona/{idZona}")
	public String eliminarZona(@PathVariable int idZona,Model model) {
		service1.deleteZona(idZona);
		return "redirect:/";
	}
}
