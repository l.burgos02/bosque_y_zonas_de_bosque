package com.crud.demo.controler;



import com.crud.demo.modelo.IncidentesDTO;
import com.crud.demo.service.ExternalApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/***
 * Aqui fui haciendo pruebas para que me entregara objetos que consumi de la api del Diego
 */
@RestController
@RequestMapping("/")
public class UseApi {

    @Autowired
    ExternalApi externalApi;

    @GetMapping("/hola")
    public String geturlbackend() {
        return externalApi.fetchUserData();
    }

    @GetMapping("/allincidente")
    public List<IncidentesDTO> getincidente() {
        return externalApi.getincidente()
                .stream()
                .map(IncidentesDTO::toIncidentes)
                .collect(Collectors.toList());
    }

}

