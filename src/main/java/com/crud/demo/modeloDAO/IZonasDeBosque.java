package com.crud.demo.modeloDAO;

import com.crud.demo.modelo.ZonasDeBosque;

import org.springframework.data.repository.CrudRepository;

public interface IZonasDeBosque extends CrudRepository<ZonasDeBosque, Integer> {
}
