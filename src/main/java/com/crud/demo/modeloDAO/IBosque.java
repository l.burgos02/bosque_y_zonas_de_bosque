package com.crud.demo.modeloDAO;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.crud.demo.modelo.Bosque;

@Repository
public interface IBosque extends CrudRepository<Bosque, Integer>{

}
